package com.greiko.camelopenapidemo;


import org.apache.camel.Exchange;
import org.apache.camel.Header;
import org.apache.camel.Headers;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.http.base.HttpOperationFailedException;
import org.springframework.stereotype.Component;

@Component
public class ExceptionHandlerRouteBuilder extends RouteBuilder {
    @Override
    public void configure() throws Exception {
        from("direct:restexception")
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(400))
                .setBody(constant("{}"))
                .end();

        from("direct:anotherException")
                .setBody(constant("{ 'test': 'another exception'}"))
                .end();


    }
}
