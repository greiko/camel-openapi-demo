package com.greiko.camelopenapidemo;


import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.http.base.HttpOperationFailedException;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.stereotype.Component;

@Component
public class FunctionalRouteBuilder extends RouteBuilder {
    @Override
    public void configure() throws Exception {

        onException(Exception.class)
                .handled(true)
                .choice()
                .when(simple("${exchangeProperty.caller} == 'rest'"))
                .to("direct:restexception")
                .otherwise()
                .to("direct:anotherException")
                .end();


        from("direct:ABC")
                .log("test")
                .to("direct:BCD")
                .end();

        from("direct:BCD")
                .process(exchange -> {
                    throw new HttpOperationFailedException("test", 500, "", "", null, null);
                })

                .end();

    }
}

