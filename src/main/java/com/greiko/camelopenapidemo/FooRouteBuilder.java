package com.greiko.camelopenapidemo;


import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.http.base.HttpOperationFailedException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class FooRouteBuilder extends RouteBuilder {
    @Override
    public void configure() throws Exception {
            from("direct:othercaller")
                    .to("direct:ABC");
    }
}
