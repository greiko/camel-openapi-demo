package com.greiko.camelopenapidemo;


import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.http.base.HttpOperationFailedException;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class RestRouteBuilder extends RouteBuilder {
    @Override
    public void configure() throws Exception {


        restConfiguration()
                .component("netty-http")
                .host("localhost")
                .port(8080)
                .bindingMode(RestBindingMode.auto)
                .dataFormatProperty("prettyPrint", "true")
                // setup context path and port number that netty will use
                .contextPath("/").port(8080)
                // add OpenApi api-doc out of the box
                .apiContextPath("/api-doc")
                .apiProperty("api.title", "User API").apiProperty("api.version", "1.2.3")
                // and enable CORS
                .apiProperty("cors", "true");


        rest("test")
                .get()
                .produces("application/json")
                .route().routeId("test")
                .setProperty("caller", constant("rest"))
                .to("direct:ABC");

        rest("other")
                .get()
                .produces("application/json")
                .route().routeId("othercaller")
                .to("direct:othercaller");

    }
}
