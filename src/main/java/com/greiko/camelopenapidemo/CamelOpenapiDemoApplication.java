package com.greiko.camelopenapidemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CamelOpenapiDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CamelOpenapiDemoApplication.class, args);
	}

}
